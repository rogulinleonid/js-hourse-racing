// showHorses() - список лошадей
// showAccount() - счёт игрока
// setWager() - сделать ставку на лошадь в следующем забеге
// startRacing() - начало забега
// newGame() - новая игра

// Задание
// 1. Напишите функцию showHorses(), которая выводит имена всех лошадей. Создайте массив лошадей.

// 2. Напишите функцию showAccount(), которая отображает текущий счёт игрока.

// 3. Напишите функцию setWager(), с помощью которой можно создать ставку - поставить сумму со счёта на конкретную лошадь. Сохраняйте ставки в массив.

// 4. Добавьте в функцию setWager() проверку, что на счету есть средства, достаточные для ставки.

// 5. Для того, чтобы скачки работали, лошади должны бегать. При этом результат должен быть разный и отложенный по времени.
// Добавьте лошадям метод run(). Этот метод должен возвращать промис, выполняющийся через случайное время, от 500 до 3000 мс.
// Для этого вам понадобятся функции new Promise(), setTimeout(), Math.random().

// 6. Напишите функцию startRacing(), которая даёт старт скачкам. Лошади должны начать бежать. Выведите в консоль каждую добежавшаю до финиша лошадь.
// Для этого вам понадобиться функция .then().

// 7. Для того, чтобы посчитать результат всех ставок, нужно узнать победителя, то есть первый выполнившийся промис.
// Сделайте это с помощью функции Promise.race().

// 8. Итог скачек можно подвести и вывести на экран после того, как все лошади доберутся до финиша, то есть все промисы выполнятся.
// Используйте функцию Promise.all(), чтобы поймать этот момент.

// 9. В качестве итога скачек выведите, сколько выиграл игрок и какой у него счёт в результате. Если он ничего не выиграл, выведите только счёт.
// Если в ставке было указана лошадь-победитель, удвойте сумму и верните на счёт.

// 10. После завершения скачки не забудьте почистить данные о ставках

// 11. Напишите функцию newGame(), дающую возможность начать игру заново.


// -----------МОЕ РЕШЕНИЕ----------

const arrayHorses = ['Люся', 'Булочка', 'СпидиГонщик', 'Жорик', 'Хрюша']

function Horse(horsesName) {
    this.place = 0
    this.horsesName = horsesName
    this.run = () => new Promise(resolve => setTimeout(() => resolve(), Math.round(Math.random() * (3000 - 500) + 500)))
}

const HorseRacing = function (money) {
    let _wagers = new Map()
    let _countRaces = 0
    let _account
    let _horses = new Array()
    let printStartInfo = function () {
        console.log('--------------Лошадиные скачки--------------------------')
        console.log('')
        console.log('Для того, что бы начать игру, напиши строку ниже в консоль')
        console.log('--------------------------------------------------------')
        console.log('let game = new Game({массив лошадей}, {твой кошелек (сумма)})')
        console.log('--------------------------------------------------------')
        console.log('если что есть уже готовый массив вороных скакунов, можешь использовать его: {arrayHorses}')
        console.log('')
    }

    class Game {
        constructor(horses, startAccount) {
            console.log('Не смог удержаться сорвать куш. Я знал.')
            console.log('Для вызова справки по игре введи команду { game.help() }')

            _account = startAccount
            for (const horse of horses)
                _horses.push(new Horse(horse))
        }

        help() {
            console.log('Описание команд:')
            console.log('game.showHorses() - показывает всех лошадей')
            console.log('game.showAccount() - показывает твоё бабло')
            console.log('game.setWager() - позволяет делать ставки, просто пиши имя лошади и сколько хочешь поставить!')
            console.log('startRacing() - команда даёт старт гонке')
            console.log('finishGame() - конец игры')
            console.log('Не тяни же, делай ставку! И будет хорошо!')
            
        }

        showHorses() {
            for (const e of _horses) {
                console.log(e.horsesName)
            }
        }

        showAccount() {
            console.log(_account)
        }

        setWager(horsesName, bet) {
            if (!arrayHorses.includes(horsesName)) {
                console.log(`Выша ставка не прошла, так как вы пытаетесь поставить на несуществующую лощадь`)
                return
            }
            if (!Number.isInteger(bet)) {
                console.log(`Выша ставка не прошла, так как вы пытаетесь поставить некорректную сумму`)
                return
            }
            if (bet <= 0) {
                console.log(`Выша ставка не прошла, так как вы пытаетесь поставить некорректную сумму`)
                return
            }
            if (_account >= bet) {
                if (_wagers.has(horsesName)) {
                    _wagers.set(horsesName, _wagers.get(horsesName) + bet)
                }
                else _wagers.set(horsesName, bet)
                _account -= bet
                console.log(`Вы успешно поставили ${bet} на лошадь ${horsesName}. Ваш текущий счет ${_account}.`)
            }
            else console.log('Ваша ставка не прошла! Вы нищеброд!')
        }

        startRacing() {
            // решил немного иначе реализовать поиск победителя. Я просто присваиваю каждой 
            // пришедшей на финиш лошади место в гонке. А затем та лошадь, которая пришла первой
            // и будет считаться победившей.
           
            let place = 1
            let promises = new Array()
            for (const horse of _horses) {
                promises.push(horse.run().then(() => {
                    horse.place = place++
                    console.log(`Лошадь ${horse.horsesName} пришла к финишу!`)
                }))
            }
            Promise.all(promises).then(() => this.getResult())
        }

        getResult() {
            const winner = _horses.find(h => h.place === 1).horsesName
            console.log('__________________________________')
            console.log(`Первой к финишу пришла лошадь ${winner}`)
            console.log('__________________________________')
            if (_wagers.has(winner)) {
               console.log('Поздравляю! Удача сегодня на Вашей стороне!')
               console.log(`Ваш выйгрыш составил: ${_wagers.get(winner) * 2}`)
               _account += _wagers.get(winner) * 2
            } else if (_wagers.length === 0) {
                console.log('Для победы нужно рисковать!')
            } else {
                console.log('В этот раз не повезло, но может повезет в другой...')
            }
            _wagers = new Map()
            _countRaces++
        }

        finishGame() {
            if (_countRaces === 0) {
                alert('Как?! Уже?!')
                alert('Но ведь ты даже не сыграл!')
                alert('Но дело твоё...')
            }
            alert(`Забери своё баблишко: ${_account}`)
            alert('Для начала новой игры нажми F5')
        }
    
    }

    return new Game(arrayHorses, money)
}

let money
let answer
alert('Добро пожаловать в игру Лошадиные скачки! Рискуй, проверь свою интуицию! И удача обязательно окажется на твоей стороне!')
while (answer !== 'да') {
    answer = prompt('Сыграем? (да/нет)', 'да')
}
while (true) {
    money = prompt('сколько денег ты готов просадить?')
    if (Number.isInteger(+money) && money > 0) {
        break
    } else {
        alert('введи корректное колличество наличности')
    }
}
let game = HorseRacing(money)
alert('Это консольная игра, так что жми скорее F12 и следуй инструкциям! Удачи!')